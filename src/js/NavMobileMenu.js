document.addEventListener('DOMContentLoaded', function() {
    const hamburger = document.getElementById('hamburger');
    const mobileMenu = document.getElementById('mobileMenu');
    const closeIcon = document.getElementById('closeIcon');
    const hamburgerIcon = document.getElementById('hamburgerIcon');
    const header = document.querySelector('header');

    function handleResize() {
        const windowWidth = window.innerWidth;
        if (windowWidth >= 768 && !mobileMenu.classList.contains('-translate-y-full')) {
            closeMenu();
        }
    }

    function closeMenu() {
        mobileMenu.classList.add('-translate-y-full');
        mobileMenu.classList.remove('translate-y-0');
        document.documentElement.style.overflow = '';
        document.body.style.overflow = '';
        header.classList.remove('bg-black');
        hamburgerIcon.classList.remove('hidden');
        closeIcon.classList.add('hidden');
        mobileMenu.classList.add('hidden');
        mobileMenu.classList.remove('flex');
    }

    hamburger.addEventListener('click', function() {
        if (mobileMenu.classList.contains('-translate-y-full')) {
            mobileMenu.classList.remove('-translate-y-full');
            mobileMenu.classList.add('translate-y-0');
            document.documentElement.style.overflow = 'hidden';
            document.body.style.overflow = 'hidden';
            header.classList.add('bg-black');
            hamburgerIcon.classList.add('hidden');
            closeIcon.classList.remove('hidden');
            mobileMenu.classList.remove('hidden');
            mobileMenu.classList.add('flex');
        } else {
            closeMenu();
        }
    });

    window.addEventListener('resize', handleResize);
    handleResize(); // Call the function on initial load to check the screen width
});
